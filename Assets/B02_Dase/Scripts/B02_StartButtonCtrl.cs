﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class B02_StartButtonCtrl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start(){}
    // Update is called once per frame
    void Update(){}
    
    // When Start Button on Menu is clicked
    public void StartButton(){
        SceneManager.LoadScene("GameScene");
    }

}
