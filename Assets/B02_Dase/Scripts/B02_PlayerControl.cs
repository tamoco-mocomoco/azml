﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B02_PlayerControl : MonoBehaviour
{
    public float speed;

    float step;
    // Start is called before the first frame update
    void Start()
    {
        speed = speed * -1;
    }

    void Update()
    {
        // When Left Mouse Button is clicked
        if (Input.GetMouseButtonDown(0))
        {
            speed = speed * -1;
        }
    }

    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 0, speed));
    }
}
