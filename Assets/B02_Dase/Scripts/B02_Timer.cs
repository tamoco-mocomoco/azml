﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class B02_Timer : MonoBehaviour {
    public float time;
    private GameObject timeText;

    // Start is called before the first frame update
    void Start () {
        this.timeText = GameObject.Find ("Time");
    }

    // Update is called once per frame
    void Update () {
        this.time -= Time.deltaTime;

        // TimeUp
        if (time < 0) {
            SceneManager.LoadScene ("B02_EndSceneFail");
        }

        string dispTime = time < 0 ? "0.0" : this.time.ToString ("F1");
        // UpdateDispTime
        this.timeText.GetComponent<Text> ().text = dispTime;
    }
}