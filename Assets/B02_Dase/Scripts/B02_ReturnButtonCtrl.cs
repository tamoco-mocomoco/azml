﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class B02_ReturnButtonCtrl : MonoBehaviour {
    public void ReturnButton () {
        SceneManager.LoadScene ("B02_OpeningScene");
    }

}