﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class B02_Goal : MonoBehaviour {
    public void Clear () {
        SceneManager.LoadScene ("B02_EndSceneSuccess");
    }

    void OnTriggerEnter2D (Collider2D col) {
        if (col.gameObject.tag == "onigiri") {
            Clear ();
        }
    }

}