﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B02_ResultControl : MonoBehaviour {
    // Start is called before the first frame update
    public float rollSpeed = 5f;
    public bool leftRoll = true;
    void Start () {
        if (!leftRoll) {
            rollSpeed *= -1;
        }

    }

    void FixedUpdate () {
        transform.Rotate (new Vector3 (0, 0, rollSpeed));
    }
}