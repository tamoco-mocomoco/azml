﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class E01_move_contllor : MonoBehaviour
{
    public float speed;
    public float timeOut;
    private float timeElapsed;
    public Vector3 pos;
    public float distance = 100f;
    public GameObject mosquito_death;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(Vector2.zero);
        Vector2 max = Camera.main.ViewportToWorldPoint(Vector2.one);
        float step = speed * Time.deltaTime;

        Transform myTtransform = this.transform;
        timeElapsed += Time.deltaTime;
        

        if (timeElapsed >= timeOut)
        {

            pos = myTtransform.position;
            pos.x = Random.Range(min.x, max.x);
            pos.y = Random.Range(min.y, max.y);
            Vector2 diff = myTtransform.position - pos;
            Vector2 scale = transform.localScale;
            if(diff.x >= 0){
                scale.x *= 1;
            }else{
                scale.x *= -1;
            }
            transform.localScale = scale;

            timeElapsed = 0.0f;
        }
        transform.position = Vector3.MoveTowards(transform.position, pos, step);

        if(Input.GetMouseButtonDown(0)){
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //RaycastHit hit = new RaycastHit();
            // if (Physics.Raycast(ray, out hit, distance)) {
            //     Destroy(gameObject , 0.0f);
            // }
            //RaycastHit2D hit = new RaycastHit2D();
            RaycastHit2D hit2d = Physics2D.Raycast((Vector2)ray.origin, (Vector2)ray.direction);
            if (hit2d) {
                if(hit2d.transform.gameObject == gameObject){
                    E01_make_mosquito.EnemyCount -= 1;
                    Destroy(gameObject,0);
                    GameObject mosquitoD = Instantiate(mosquito_death);
                    mosquitoD.transform.position = this.transform.position;
                    if (E01_make_mosquito.EnemyCount <= 0){
                        SceneManager.LoadScene ("E01_GameClear");
                    }
                }
            }
        }
    }

}
