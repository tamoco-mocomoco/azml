﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E01_make_mosquito : MonoBehaviour
{
    public GameObject Mosquito;
    public static int EnemyCount;
    //bool instantMode = false;
    // Start is called before the first frame update
    void Start()
    {
        EnemyCount = 5;
        int i;
        for (i = 0; i < EnemyCount; i++){
            Vector2 pos;
            Vector2 min = Camera.main.ViewportToWorldPoint(Vector2.zero);
            Vector2 max = Camera.main.ViewportToWorldPoint(Vector2.one);
            pos.x = Random.Range(min.x, max.x);
            pos.y = Random.Range(min.y, max.y);
            GameObject kasan = Instantiate(Mosquito) as GameObject;
            kasan.transform.position = pos;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
