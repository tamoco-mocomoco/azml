﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class E01_timelimit : MonoBehaviour
{
    private float time = 10;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = ((int)time).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //1秒に1ずつ減らしていく
        time -= Time.deltaTime;
        //マイナスは表示しない
        if (time < 0) time = 0;
        GetComponent<Text>().text = ((int)time).ToString();
        if (time == 0)
        {
            SceneManager.LoadScene("E01_GameOver");
        }
    }
}
