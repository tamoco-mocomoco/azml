﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E01_tap_effect : MonoBehaviour
{
    public GameObject tapeffect;
    public float Timer = 1.0f;

    void Start()
    {
        //Destroy(gameObject, 1.3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var mousePosition = Input.mousePosition;
            mousePosition.z = 1;
            var pos = Camera.main.ScreenToWorldPoint(mousePosition);
            GameObject effect1 = Instantiate(tapeffect);
            effect1.transform.position = pos;

            Destroy(effect1,Timer);
        }
    }
}
