﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButterflyMovement : MonoBehaviour
{
    const string TARGET_NAME = "butterfly";
    string targetName = "";
    Rigidbody2D rb;
    float x = 0;
    float direction = 0;
    static int enemyCounta;

    private float changeTime = 0.3f;
    private float timeElapsed;
    private float timeOver;
    private float limitTime = 20;
    private float thrust = 10;
    private const float minSpeed = -20;
    private const float maxSpeed = 20;

    public Text time;

    // Start is called before the first frame update
    void Start()
    {
        enemyCounta = 5;
        targetName = gameObject.name;
        rb = gameObject.GetComponent<Rigidbody2D>();
        transform.Rotate(transform.position.x, transform.position.y, direction);
        rb.AddForce(new Vector2(Random.Range(minSpeed, maxSpeed), Random.Range(minSpeed, maxSpeed)), ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        direction = Random.Range(0, 360);

        //Vector3 pos = new Vector3(x,0,0);
        //gameObject.transform.rotation = new Quaternion(this.transform.position.x, this.transform.position.y, this.transform.position.z, direction);
        //gameObject.transform.rotation = new Quaternion(this.transform.position.x, this.transform.position.y, this.transform.position.z, direction);
        timeElapsed += Time.deltaTime;
        timeOver += Time.deltaTime;
        time.text = (limitTime - timeOver).ToString("F2");
        if (timeOver >= limitTime)
        {
            SceneManager.LoadScene("GameOverScene");
        }
        if (timeElapsed >= changeTime)
        {
            //transform.Rotate(transform.position.x, transform.position.y, direction);
            timeElapsed = 0;
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(new Vector2(Random.Range(minSpeed, maxSpeed), Random.Range(minSpeed, maxSpeed)), ForceMode2D.Impulse);
        }
        Debug.Log(transform.up);

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = new Ray();
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

            //マウスクリックした場所からRayを飛ばし、オブジェクトがあればtrue 
            if (hit)
            {
                Debug.Log("raycast");

                if (hit.collider.gameObject.name == targetName)
                {

                    Destroy(gameObject);
                    
                    enemyCounta -= 1;
                    Debug.Log(enemyCounta + "匹だよ！");
                    if (enemyCounta <= 0)
                    {
                        SceneManager.LoadScene("ClearScene");
                    }
                }
            }
        }
    }
}
