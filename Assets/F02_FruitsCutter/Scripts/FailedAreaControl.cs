﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailedAreaControl : MonoBehaviour
{
    private int failCnt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Destroy(other.gameObject);
        failCnt +=1;
        if (failCnt >= 10) {
            Debug.Log("Game Over !!");
        }
    }
}
