﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class F02_TimeCount : MonoBehaviour {
    //public GameObject timeObj;
    float timeCouta = 10;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void FixedUpdate () {
        Text timeText = gameObject.GetComponent<Text> ();
        timeCouta -= Time.deltaTime;
        //Debug.Log (timeCouta);
        timeText.text = timeCouta.ToString ("00.00");
        if (timeCouta <= 0) {
            F02_Result.resultText = "NotClear";
            SceneManager.LoadScene ("F02_Result");　
        }

        if (F02_FruitsCount.fruitsCouta <= 0) {
            F02_Result.resultText = "GameClear";
            SceneManager.LoadScene ("F02_Result");　
        }

    }
}