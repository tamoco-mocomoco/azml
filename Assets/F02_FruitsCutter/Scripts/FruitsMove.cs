﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitsMove : MonoBehaviour
{
    public float power = 15f;
    private int rotation;
    Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        rb2d =  gameObject.GetComponent<Rigidbody2D>();
        GameObject targetObj = GameObject.Find("Center");
        var vec = (targetObj.transform.position - gameObject.transform.position).normalized;
        gameObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, vec);
        rb2d.AddForce(transform.up * power, ForceMode2D.Impulse);
        int x = (int)gameObject.transform.position.x * 10;
        rotation = x;
        Destroy (gameObject, 30.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 0, rotation) * Time.deltaTime);
    }
}
