﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F02_Result : MonoBehaviour {
    public static string resultText;
    // Start is called before the first frame update
    void Start () {
        Text resultUIText = gameObject.GetComponent<Text> ();

        resultUIText.text = resultText.ToString ();

    }

}