﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F02_FruitsCount : MonoBehaviour {
    Text fruitsText;
    public static int fruitsCouta;
    // Start is called before the first frame update
    void Start () {
        fruitsCouta = Random.Range (10, 15);
    }

    // Update is called once per frame
    void Update () {
        Text fruitsText = gameObject.GetComponent<Text> ();

        fruitsText.text = fruitsCouta.ToString ();
    }
}