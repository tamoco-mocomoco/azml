﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectControl : MonoBehaviour {
    private int targetNum = 10;
    private int cutCnt;
    // Start is called before the first frame update
    void Start () {
        Destroy (gameObject, 0.1f);
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnTriggerEnter2D (Collider2D other) {
        F02_FruitsCount.fruitsCouta--;
        GameControl.cutCnt += 1;
        Debug.Log (GameControl.cutCnt + " : " + targetNum);
        if (GameControl.cutCnt >= targetNum) {
            Debug.Log ("Game Clear !!");
        }
        Destroy (other.gameObject);
    }
}