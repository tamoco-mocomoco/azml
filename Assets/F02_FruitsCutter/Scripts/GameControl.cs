﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    static public int cutCnt;

    public Sprite[] fruitsImgList;
    public GameObject fruits;
    public GameObject effect;
    public float timeAppear;
    private float timeElapsed;

    public float cutTime = 0.5f;
    private float cutTimeElapsed;

    private GameObject appearObj;

    // Start is called before the first frame update
    void Start()
    {
        cutCnt = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timeElapsed += Time.deltaTime;

        if(timeElapsed >= timeAppear){

            appearObj = Instantiate(fruits, new Vector3(Random.Range(-4.5f, 4.5f), -10, 0), Quaternion.identity);
            appearObj.GetComponent<SpriteRenderer>().sprite = fruitsImgList[Random.Range(0,fruitsImgList.Length)];

            timeElapsed = 0.0f;

        }
        if(Input.GetMouseButtonDown(0)){
            cutTimeElapsed = 0.0f;
        }
        if(Input.GetMouseButton(0)){
            if(cutTimeElapsed <= cutTime){
                cutTimeElapsed += Time.deltaTime;
                Vector3 screenPos = Input.mousePosition;
                Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
                worldPos.z = 0;
                Instantiate(effect, worldPos, Quaternion.identity);
            }


        }

    }
}
