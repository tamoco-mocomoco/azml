﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class A02_Result : MonoBehaviour {

    public static bool gameResult = true;

    public GameObject[] successOjbs;
    public GameObject[] failOjbs;

    void Start () {
        // if (gameObject.GetComponent<Text> ()) {
        //     //シーン越しで値を渡す最もカンタンな方法
        //     //staticの値なら参照対象が単一でアタッチしなくても参照可能
        //     gameObject.GetComponent<Text> ().text = A02_ClearCheck.gameResultText;
        // }
        if (gameResult) {
            foreach (GameObject obj in successOjbs) {
                Instantiate (obj, obj.transform.position, obj.transform.rotation);
            }
        } else {
            foreach (GameObject obj in failOjbs) {
                Instantiate (obj, obj.transform.position, obj.transform.rotation);
            }

        }

    }
    // Start is called before the first frame update

}