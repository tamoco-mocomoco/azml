﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class A02_ClearCheck : MonoBehaviour {

    public GameObject parentOjb;

    public GameObject parentSuccessEffect;

    List<GameObject> checkObjList = new List<GameObject> ();
    List<GameObject> effectObjList = new List<GameObject> ();
    public static string gameResultText = "";

    [SerializeField] Text limitTimeText;

    public float setLimitTime = 6f;

    public float waitTime = 1.5f;

    public string nextSceanName = "A02_Result";

    bool gameEnd = false;

    void Start () {
        A02_Result.gameResult = false;
        //子供のゲームオブジェクトをリストに格納
        foreach (Transform childObj in parentOjb.transform) {
            //親から子に対し非アクティブでも取得してしまうため
            //アクティブなオブジェクトのみ対象にする
            if (childObj.gameObject.activeSelf) {
                Debug.Log ("突っ込みました：" + childObj.gameObject.name);
                checkObjList.Add (childObj.gameObject);
            }
        }

        foreach (Transform effectObj in parentSuccessEffect.transform) {
            effectObjList.Add (effectObj.gameObject);
        }
        Debug.Log (effectObjList);

    }

    // Update is called once per frame
    void Update () {
        if (!gameEnd) {
            setLimitTime -= Time.deltaTime;
        }
        //(int)で型をキャストできます（文字列操作のときに便利）
        limitTimeText.text = ((int) setLimitTime).ToString ();

        if (setLimitTime < 0) {
            gameEnd = true;
            gameResultText = "時間ぎれ！";
            A02_Result.gameResult = false;

            waitTime -= Time.deltaTime;
            if (waitTime < 0) {
                SceneManager.LoadScene (nextSceanName);
            }
        }

        if (ClearCheack ()) {
            gameEnd = true;
            gameResultText = "全拭きオッケー！";

            foreach (GameObject effect in effectObjList) {
                effect.SetActive (true);
            }

            A02_Result.gameResult = true;
            waitTime -= Time.deltaTime;
            if (waitTime < 0) {
                SceneManager.LoadScene (nextSceanName);
            }
        }
    }

    public bool ClearCheack () {
        foreach (GameObject obj in checkObjList) {
            if (obj) {
                return false;
            }
        }
        return true;
    }

}