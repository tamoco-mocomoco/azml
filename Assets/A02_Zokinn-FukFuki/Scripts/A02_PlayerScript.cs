﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class A02_PlayerScript : MonoBehaviour {
    float x;
    float y;

    void Update () {
        x = Input.mousePosition.x;
        y = Input.mousePosition.y;
    }

    void OnMouseDrag () {
        Debug.Log ("どらっぐ！");
        transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (x, y, 10.0f));
    }

    private void OnCollisionEnter2D (Collision2D col) {
        Destroy (col.gameObject);
    }

}