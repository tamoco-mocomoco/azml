﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D03_PitcherScript : MonoBehaviour {
    public GameObject ball;
    float waitTime = 0;
    float timeCount = 0;

    bool isThrow = false;
    // Start is called before the first frame update
    void Start () {
        waitTime = Random.Range (0.5f, 1f);

        ball.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (1.5f, 2.5f);
    }

    // Update is called once per frame
    void Update () {
        timeCount += Time.deltaTime;
        if (waitTime < timeCount && !isThrow) {
            GameObject instBall = Instantiate (ball, ball.transform.position, ball.transform.rotation);
            instBall.name = "Ball";
            isThrow = true;
        }
    }
}