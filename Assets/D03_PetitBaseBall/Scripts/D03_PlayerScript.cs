﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D03_PlayerScript : MonoBehaviour {

    public float rotate_speed = 10;
    float battingTime = 0;
    bool isTouch = false;

    // Start is called before the first frame update
    void Start () {
        rotate_speed *= -1;
    }

    // Update is called once per frame
    void Update () {

        if (isTouch) {
            battingTime += Time.deltaTime;
            if (battingTime < 0.4f) {
                transform.Rotate (new Vector3 (0, 0, rotate_speed));
            }

        }

        if (Input.GetMouseButtonDown (0)) {
            isTouch = true;
        }

    }

}