﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class D03_GameSuccess : MonoBehaviour {
    public GameObject resultChara;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.name == "Ball") {
            //Instantiate (resultChara, resultChara.transform.position, resultChara.transform.rotation);
            Destroy (col.gameObject);
            Debug.Log ("ゲームクリアー");
            D03_ResultScript.isGameClear = true;
            SceneManager.LoadScene ("D03_Result");
        }
    }

}