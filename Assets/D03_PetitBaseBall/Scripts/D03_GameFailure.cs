﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class D03_GameFailure : MonoBehaviour {
    // Start is called before the first frame update

    public GameObject resultChara;
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.name == "Ball") {
            //Instantiate (resultChara, resultChara.transform.position, resultChara.transform.rotation);

            Debug.Log ("ノットクリア");
            Destroy (col.gameObject);
            D03_ResultScript.isGameClear = false;
            SceneManager.LoadScene ("D03_Result");
        }
    }

}