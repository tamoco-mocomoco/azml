﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class D03_ResultScript : MonoBehaviour {
    public static bool isGameClear;

    public GameObject notClearObj;
    public GameObject clearObj;

    // Start is called before the first frame update
    void Start () {
        if (isGameClear) {
            gameObject.GetComponent<Camera> ().backgroundColor = Color.cyan;
            clearObj.SetActive (true);
        } else {
            gameObject.GetComponent<Camera> ().backgroundColor = Color.magenta;
            notClearObj.SetActive (true);
        }
    }

}