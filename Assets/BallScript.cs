﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//球の速さ（X,Y）
//Xについては位置によって負の方向へ
//クリックされたかどうか

//プレハブにつけるものなので、
//基本的に設定数値は全部プライベートです。

public class BallScript : MonoBehaviour {

	//Rigidbody2Dを使うための変数
	static Rigidbody2D rb;
	static GameObject BallObject;
	static string uniqID;

	//フェードアウト一式
	private float fadeTime = 0.6f;
	private float currentRemainTime;
	private SpriteRenderer spRenderer;

	//初期値 (spX:0.5,spY:6.0Y)
	static float spX = 0.5f;
	float spY = 5.0f;

	//オブジェクトが自動的に削除される間隔：1.5f
	private float destroyTime = 1.6f;

	//カーブするための設定値：1.8f
	private float curvePower = 1.8f;

	//各方向の乱数
	float spXR;
	float spYR;

	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		moveChange ();

		// 初期化
		currentRemainTime = fadeTime;
		spRenderer = GetComponent<SpriteRenderer> ();

		//Debug.Log ("俺のインスタンスID" + GetHashCode ());なごり
		//		string adt = gameObject.GetInstanceID().ToString();
		//		Debug.Log(adt);
		// フェード
		Invoke ("Fadeout", destroyTime);

	}

	//なんか当たったら処理（球同士の接触）
	//	void OnCollisionEnter2D(Collision2D col){
	//			Destroy(gameObject, 0);
	//		}

	void moveChange () {
		//速さの振れ幅を変える。特にXは角度に影響。
		spXR = Random.Range (1.0f, 2.0f);
		spYR = Random.Range (1.0f, 2.0f);

		float x = gameObject.transform.position.x;
		if (x >= 0) {
			spX = spX * -1;
			curvePower = curvePower * -1;
		}
	}

	//	static public float XmoveChange (float x){
	//		//速さの振れ幅を変える。特にXは角度に影響。
	//		spXR = Random.Range (1.0f,  2.0f);
	//		spYR = Random.Range (1.0f , 2.0f);
	//
	//		//左側だったら右方向へ、右側だったら左方向へ
	//		if (x >= 0){
	//			spX = spX * -1;
	//		}
	//		return x;
	//	}

	// Update is called once per frame
	void Update () {

		//移動し続ける
		rb.velocity = new Vector2 (spX * spXR + curvePower, spY * spYR);

		// // 以下オブジェクトがクリックされた時の処理 || オブジェクトの名前を取得できる（便利）
		// GameObject clickObj = ObjectTouch.getClickObject();
		// //		Debug.Log (clickObj);
		// if (clickObj != null) {
		// 	CheckTouch.checkTouch (clickObj);
		// 	//ScoreManager.score = ScoreManager.score + 1;
		// 	Destroy (gameObject);
		// }

		//フェードアウト処理までの時間
		//Fadeout ();
		Invoke ("Fadeout", 1.0f);
	}

	void Fadeout () {
		// 残り時間を更新
		currentRemainTime -= Time.deltaTime;

		if (currentRemainTime <= 0f) {
			// 残り時間が無くなったら自分自身を消滅
			GameObject.Destroy (gameObject);
			return;
		}

		// フェードアウト
		float alpha = currentRemainTime / fadeTime;
		var color = spRenderer.color;
		color.a = alpha;
		spRenderer.color = color;
	}

}