﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class B03_PlayerScript : MonoBehaviour {
    public GameObject postObj;
    public GameObject letterObj;

    public GameObject[] heartsArr;
    private bool isJumpRequest;

    public float power = 5;

    // プレイヤーからの入力はFixedではなく通常のUpdateに記述すること
    void Update () {
        if (Input.GetMouseButtonDown (0)) {
            Debug.Log ("クリックされたよ♪");

            isJumpRequest = true;
            //Instantiate (letterObj, transform.position, transform.rotation);
        }
    }

    //当たって消える処理はアタッチしている自分のオブジェクトのみで当たった側は制御しない
    private void OnCollisionEnter2D (Collision2D col) {

        if (col.gameObject == postObj) {
            Debug.Log ("ポストに当たりましたよ！");
            Instantiate (letterObj, transform.position, transform.rotation);

            foreach (GameObject heart in heartsArr) {
                heart.SetActive (true);
            }
            //Destroy (col.gameObject);
            B03_ResultScript.isGameClear = true;
            Invoke ("NextScene", 2f);
            gameObject.SetActive (false);

        } else if (col.gameObject.name != "Ground") {
            B03_ResultScript.isGameClear = false;
            Invoke ("NextScene", 2f);
            gameObject.SetActive (false);
        }

    }

    private void NextScene () {
        SceneManager.LoadScene ("B03_Result");　
    }

    void FixedUpdate () {
        if (isJumpRequest) {
            isJumpRequest = false;
            gameObject.GetComponent<Rigidbody2D> ().velocity = Vector3.up * power;
        }
    }

}