﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B03_EnemyScript : MonoBehaviour {
    public float speed = 3;
    public bool rightMode = true;
    // Start is called before the first frame update
    void Start () {
        if (rightMode == false) {
            speed = speed * -1;
        }
        Destroy (gameObject, 3);
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter2D (Collision2D col) {
        Debug.Log ("矢が「" + col.gameObject.name + "」に当たりました");
        Destroy (gameObject);
    }

    void FixedUpdate () {
        Rigidbody2D rb = this.GetComponent<Rigidbody2D> (); // rigidbodyを取得
        Vector3 force = new Vector3 (speed, 0.0f, 0.0f); // 力を設定
        rb.AddForce (force, ForceMode2D.Impulse); // 力を加える
    }

}