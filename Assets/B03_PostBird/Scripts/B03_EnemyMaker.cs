﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B03_EnemyMaker : MonoBehaviour {
    public float timeOut = 3f;
    private float timeElapsed;
    public GameObject arrowObj;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        timeElapsed += Time.deltaTime;

        if (timeElapsed >= timeOut) {
            // Do anything
            //timeOut = Random.Range (2f, 4f);
            Instantiate (arrowObj, transform.position, arrowObj.transform.rotation);
            timeElapsed = 0.0f;
        }
    }
}