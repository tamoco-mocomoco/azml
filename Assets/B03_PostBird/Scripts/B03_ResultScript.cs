﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class B03_ResultScript : MonoBehaviour {
    public static bool isGameClear;

    public GameObject uiTextTitle;
    public GameObject uiTextContent;

    public GameObject notClearObj;
    public GameObject clearObj;

    // Start is called before the first frame update
    void Start () {
        if (isGameClear) {
            uiTextTitle.GetComponent<Text> ().text = "Clear！";
            uiTextContent.GetComponent<Text> ().text = "無事に手紙を届ける\nことが出来ました";
            clearObj.SetActive (true);
        } else {
            uiTextTitle.GetComponent<Text> ().text = "Not Clear！";
            uiTextContent.GetComponent<Text> ().text = "残念ながら手紙を\n届けれませんでした";
            notClearObj.SetActive (true);
        }
    }

}