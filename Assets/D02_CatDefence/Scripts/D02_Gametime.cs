﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class D02_Gametime : MonoBehaviour
{

    public float gametime = 5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gametime -= Time.deltaTime;
        if (gametime > 0) {
            GetComponent<Text>().text = gametime.ToString("F2");
        } else
        {
            GetComponent<Text>().text = "0.00";
        }

        if (gametime <= 0)
        {
            SceneManager.LoadScene("D02_Clear");
        }
    }

}
