﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D02_Cat_move : MonoBehaviour
{
    public float speed;

    private Vector3 targetpos = new Vector3(0, -3.5f, 0);

    // Start is called before the first frame update
    void Start()
    {
        targetpos.x = Random.Range(-2.5f, 2.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if ((targetpos - transform.position).magnitude > -0.5f && (targetpos - transform.position).magnitude < 0.5f )
        {
            targetpos.x = Random.Range(-2.5f, 2.5f);
            if(targetpos.x - transform.position.x > 0)
            {
                Debug.Log(targetpos.x - transform.position.x);
                transform.rotation = Quaternion.Euler(0, 0, 0);
            } else
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }else{
            transform.position = Vector3.Lerp(transform.position, targetpos, Time.deltaTime * speed);
        }
    }
}
