﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class D02_GameOver_Controller : MonoBehaviour {
    public float interval = 0.1f;
    public float timeLimit = 5f;
    private float timediff;
    private bool exitSwitch = false;
    Renderer renderComponent;

    // Start is called before the first frame update
    void Start () {
        Debug.Log ("controller_start");
        renderComponent = GetComponent<Renderer> ();
        timediff = timeLimit;
    }

    // Update is called once per frame
    void Update () {
        if (exitSwitch) {

            timeLimit -= Time.deltaTime;
            GetComponent<Renderer> ().material.color = new Color32 (255, (byte) (255 * (timeLimit / timediff)), (byte) (255 * (timeLimit / timediff)), 255);
            if (timeLimit <= 0) {
                SceneManager.LoadScene ("D02_Gameover");
            }
        }
        Debug.Log (timeLimit);
    }

    private void OnTriggerExit2D (Collider2D collision) {
        exitSwitch = true;
        StartCoroutine ("Blink");
    }
    private void OnTriggerEnter2D (Collider2D collision) {
        exitSwitch = false;
        StopCoroutine ("Blink");
        renderComponent.enabled = true;

    }

    IEnumerator Blink () {
        while (true) {

            renderComponent.enabled = !renderComponent.enabled;
            yield return new WaitForSeconds (interval);
        }
    }
}