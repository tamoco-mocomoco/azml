﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D02_kasa_move : MonoBehaviour
{
    private Vector3 playerPos;
    private Vector3 mousePos;



    // Update is called once per frame
    void Update()
    {
        playerControl();
    }

    private void playerControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            playerPos = this.transform.position;
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 prePos = this.transform.position;
            Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - mousePos;

            //タッチ対応デヴァイス向け、1本指にのみ反応
            if(Input.touchSupported)
            {
                diff = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position) - mousePos;
            }

            diff.z = 0.0f;
            this.transform.position = playerPos + diff;    
        }

        if (Input.GetMouseButtonUp(0))
        {
            playerPos = Vector3.zero;
            mousePos = Vector3.zero;
        }
    }
    
}
