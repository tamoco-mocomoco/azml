﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class A01_ClearCheck : MonoBehaviour {
    // Start is called before the first frame update
    //public GameObject[] checkObjArray;
    public GameObject ballParentObj;

    List<GameObject> checkObjList = new List<GameObject> ();
    public static string gameResultText = "";

    [SerializeField] Text limitTimeText;

    public float setLimitTime = 6f;

    void Start () {
        //子供のゲームオブジェクトをリストに格納
        foreach (Transform childObj in ballParentObj.transform) {
            //親から子に対し非アクティブでも取得してしまうため
            //アクティブなオブジェクトのみ対象にする
            if (childObj.gameObject.activeSelf) {
                Debug.Log ("突っ込みました：" + childObj.gameObject.name);
                checkObjList.Add (childObj.gameObject);
            }
        }
    }

    // Update is called once per frame
    void Update () {
        setLimitTime -= Time.deltaTime;
        //(int)で型をキャストできます（文字列操作のときに便利）
        limitTimeText.text = ((int) setLimitTime).ToString ();

        if (setLimitTime < 0) {
            gameResultText = "時間ぎれ！";
            SceneManager.LoadScene ("A01_Result");
        }

        if (ClearCheack ()) {
            gameResultText = "全消しクリア！";
            SceneManager.LoadScene ("A01_Result");
        }
    }

    public bool ClearCheack () {
        foreach (GameObject obj in checkObjList) {
            if (obj) {
                return false;
            }
        }
        return true;
    }

    public bool IsChildObjects () {
        foreach (GameObject childObj in transform) {
            if (childObj) {
                return false;
            }
        }
        return true;
    }
}