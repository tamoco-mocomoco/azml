﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class A01_TouchScript : MonoBehaviour {

    // Update is called once per frame
    void Update () {
        if (IsTouchObj (gameObject)) {
            Destroy (gameObject);
        }
    }

    bool IsTouchObj (GameObject targetObj) {
        if (Input.GetMouseButtonDown (0)) {　
            Vector3 aTapPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);　
            Collider2D aCollider2d = Physics2D.OverlapPoint (aTapPoint);　
            if (aCollider2d) {

                GameObject obj = aCollider2d.transform.gameObject;
                if (targetObj == aCollider2d.transform.gameObject) {
                    Debug.Log (obj.name);
                    return true;
                }
            }
        }
        return false;
    }

}