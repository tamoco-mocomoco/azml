﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class A01_Result : MonoBehaviour {

    void Start () {
        if (gameObject.GetComponent<Text> ()) {
            //シーン越しで値を渡す最もカンタンな方法
            //staticの値なら参照対象が単一でアタッチしなくても参照可能
            gameObject.GetComponent<Text> ().text = A01_ClearCheck.gameResultText;
        }
    }
    // Start is called before the first frame update

}