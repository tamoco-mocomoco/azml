﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class C02_Result : MonoBehaviour {
    public static bool isGameClear = true;
    public GameObject successChara;
    public GameObject failureChara;
    public GameObject resultText;
    // Start is called before the first frame update
    void Start () {
        if (isGameClear) {
            GameObject sucessObj = Instantiate (
                successChara,
                successChara.transform.position,
                successChara.transform.rotation
            );
            resultText.GetComponent<Text> ().text = "鼻に突っ込めた！";

        } else {
            GameObject failerObj = Instantiate (
                failureChara,
                failureChara.transform.position,
                failureChara.transform.rotation
            );
            resultText.GetComponent<Text> ().text = "ザンネン空振りです";

        }

    }

    // Update is called once per frame
    void Update () {

    }
}