﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class C02_HandMove : MonoBehaviour {
    float rightMax = 2.0f; //左へ移動可能 (x)最大値
    float leftMax = -2.0f; //右へ移動可能 (x)最大値
    float currentPositionX; //現在の位置(x)保存
    float currentPositionY; //現在の位置(y)保存
    public float sideMoveSpeed = 3.0f; //移動速度+方向

    public float pushSpeed = 10.0f; //打ち出されるスピード

    bool isTouched = false; //タッチされたか
    bool isGameEnd = false; //タッチ後、ゲーム完了したか

    private float stayTime = 0;

    public string nextSceneName = "C02_Result";
    public GameObject successChara;
    public GameObject failureChara;


    List<GameObject> colList = new List<GameObject> ();

    void Start () {
        currentPositionX = transform.position.x;
        currentPositionY = transform.position.y;
    }
    void Update () {
        if (!isTouched) {
            currentPositionX += Time.deltaTime * sideMoveSpeed;
            if (currentPositionX >= rightMax) {
                sideMoveSpeed *= -1;
                currentPositionX = rightMax;
            }
            //現在の位置(x) 右へ移動可能 (x)最大値より大きい、もしくは同じの場合
            //移動速度+方向-1を掛けて反転、現在の位置を右へ移動可能 (x)最大値に設定
            else if (currentPositionX <= leftMax) {
                sideMoveSpeed *= -1;
                currentPositionX = leftMax;
            }
            transform.position = new Vector3 (currentPositionX, currentPositionY, 0);
        }

        TouthPushHand ();

        if (isTouched) {
            stayTime += Time.deltaTime;
            if (stayTime > 1.0f) {
                if (colList.Count == 1) {
                    foreach (GameObject checkObj in colList) {
                        if (checkObj.name == "PushHole") {
                            C02_Result.isGameClear = true;
                        } else {
                            C02_Result.isGameClear = false;
                            //ResultCharaCreate (false);
                        }
                    }
                }
                if (colList.Count > 1) {
                    C02_Result.isGameClear = true;
                    //ResultCharaCreate (true);
                }
                if (colList.Count == 0) {
                    C02_Result.isGameClear = false;
                    //ResultCharaCreate (false);
                }
                SceneManager.LoadScene (nextSceneName);
            }
        }
    }

    //同じ画面にゲームクリア成否のキャラを出すときは以下のメソッドを使う
    void ResultCharaCreate (bool result) {
        if (result && !GameObject.Find (successChara.name)) {
            GameObject sucessObj = Instantiate (
                successChara,
                successChara.transform.position,
                successChara.transform.rotation
            );
            sucessObj.name = successChara.name;
            if (GameObject.Find (failureChara.name)) {
                Destroy (GameObject.Find (failureChara.name));
            }

        } else if (!GameObject.Find (successChara.name) && !GameObject.Find (failureChara.name)) {
            GameObject failerObj = Instantiate (
                failureChara,
                failureChara.transform.position,
                failureChara.transform.rotation
            );
            failerObj.name = failureChara.name;
        }
    }

    void TouthPushHand () {
        Vector3 PushForse = new Vector3 (0, pushSpeed, 0);
        if (Input.GetMouseButtonDown (0)) {
            gameObject.GetComponent<Rigidbody2D> ().AddForce (PushForse, ForceMode2D.Impulse);
            isTouched = true;
        }
    }

    private void OnCollisionEnter2D (Collision2D col) {
        colList.Add (col.gameObject);
    }

}