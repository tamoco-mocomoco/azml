﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Bo1_timelimit : MonoBehaviour {
	private float gameTime = 4;
	public float waitTime = 1.5f;

	void Start () {
		//初期値60を表示
		//float型からint型へCastし、String型に変換して表示
		GetComponent<Text> ().text = ((int) gameTime).ToString ();
	}

	void Update () {
		//1秒に1ずつ減らしていく
		gameTime -= Time.deltaTime;
		//マイナスは表示しない
		if (gameTime < 0) {
			gameTime = 0;
		}
		GetComponent<Text> ().text = ((int) gameTime).ToString ();

		if (gameTime == 0) {
			SceneManager.LoadScene ("B01_GameOver");
		}
	}

}