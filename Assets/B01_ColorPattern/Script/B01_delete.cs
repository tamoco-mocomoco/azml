﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class B01_delete : MonoBehaviour {

    private int ClickCount = 0;
    private int point = 0;
    // Start is called before the first frame update
    void Start () {
        point = 0;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown (0)) {
            Vector2 touchPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            Collider2D targetCollider = Physics2D.OverlapPoint (touchPos);
            //タグによる検知をやめてnameの中に文字が含まれているかで検知
            //if (targetCollider.gameObject.tag == "B01_Red")
            if (targetCollider.gameObject.name.Contains ("Red")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Red");
                point += 1;
                ClickCount += 1;
            } else if (targetCollider.gameObject.name.Contains ("Green")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Green");
                point += 10;
                ClickCount += 1;
            } else if (targetCollider.gameObject.name.Contains ("Blue")) {
                Destroy (targetCollider.gameObject);
                Debug.Log ("Tag=Blue");
                point += 100;
                ClickCount += 1;
            }
        }
        if (ClickCount == 3) {
            Debug.Log ("GAME SET");

            if (point == 3) //赤のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameClear");
            } else if (point == 30) //緑のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameClear");
            } else if (point == 300) //青のカウント
            {
                Debug.Log ("ゲームクリア！");
                SceneManager.LoadScene ("B01_GameClear");
            } else {
                Debug.Log ("ゲームオーバー");
                SceneManager.LoadScene ("B01_GameOver");
            }
        };

    }

}