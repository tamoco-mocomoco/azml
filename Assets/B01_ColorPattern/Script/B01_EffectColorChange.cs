﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B01_EffectColorChange : MonoBehaviour {

    public Color[] colorSet = { Color.cyan, Color.magenta, Color.yellow };
    // Start is called before the first frame update
    void Start () {
        if (colorSet.Length == 0) {
            gameObject.GetComponent<SpriteRenderer> ().material.SetColor ("_Color", Color.white);
        } else {
            gameObject.GetComponent<SpriteRenderer> ().material.SetColor ("_Color", colorSet[Random.Range (0, colorSet.Length)]);
            //targetObj.GetComponent<SpriteRenderer> ().material.SetColor ("_Color", Color.cyan);
        }
    }

    // Update is called once per frame
    void Update () {

    }

}