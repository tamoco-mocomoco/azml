﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B01_ResultEffect : MonoBehaviour {

    public GameObject effectObj;
    float colWait = 0.1f;
    public bool colCheck = false;

    float[] posXlist = {-2, 0, 2 };

    float cycleTime = 0.3f;
    float waitTime = 0.3f;

    float defaultPosX, defaultPosY, defaultPosZ;

    void Start () {
        defaultPosX = transform.position.x;
        defaultPosY = transform.position.y;
        defaultPosZ = transform.position.z;
    }

    void Update () {
        //1秒に1ずつ減らしていく
        waitTime -= Time.deltaTime;
        //マイナスは表示しない
        if (waitTime < 0) {
            GameObject targetObj = Instantiate (
                effectObj,
                //new Vector3 (posXlist[Random.Range (0, posXlist.Length)], 6, 0),
                new Vector3 (Random.Range (-2.1f, 2.1f), 6, 0),
                effectObj.transform.rotation
            );

            Destroy (targetObj, 3);
            waitTime = cycleTime;
        }

        if (colCheck) {
            colWait -= Time.deltaTime;
            if (colWait < 0) {
                transform.position = new Vector3 (
                    defaultPosX, defaultPosY, defaultPosZ
                );
                colCheck = false;
                colWait = 0.1f;
            }
        }
    }

    private void OnCollisionEnter2D (Collision2D col) {
        transform.position = new Vector3 (
            defaultPosX, defaultPosY - 0.2f, defaultPosZ
        );
        colCheck = true;
    }

}