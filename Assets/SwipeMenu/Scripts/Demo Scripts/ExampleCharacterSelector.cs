﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// A simple example script. Updates text in the demo to show a menu item has been selected.
/// </summary>
public class ExampleCharacterSelector : MonoBehaviour {
	public Text text;

	void Start () {
		FadeManager.FadeIn ();
	}
	public void Select (string name) {
		Debug.Log ("clicked!!::" + name);
		text.text = (name + " selected");
		FadeManager.FadeOut (name);
		//SceneManager.LoadScene (name);
	}
}