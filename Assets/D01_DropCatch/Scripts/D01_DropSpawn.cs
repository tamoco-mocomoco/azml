﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D01_DropSpawn : MonoBehaviour
{

    // あめ prefab
    [SerializeField] private GameObject dropObject;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("dropStart");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator dropStart()
    {
        yield return new WaitForSeconds(Random.Range(1, 5));


        Instantiate(dropObject, transform.position, Quaternion.identity);


        StartCoroutine("dropStart");

    }
}
