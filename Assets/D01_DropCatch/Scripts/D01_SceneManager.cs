﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class D01_SceneManager : MonoBehaviour {
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    public void MoveTitle () {
        SceneManager.LoadScene ("D01_Title");
    }

    public void MovePlay () {
        SceneManager.LoadScene ("D01_Play");
    }
}