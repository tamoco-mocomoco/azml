﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class D01_DropCatcher : MonoBehaviour {
    // ゲームクリア条件
    [SerializeField] private int successLineCount;

    // カウンターのテキスト
    private Text counterText;

    // あめ獲得数
    private int count;

    // 位置座標
    private Vector3 position;
    // スクリーン座標をワールド座標に変換した位置座標
    private Vector3 screenToWorldPointPosition;

    // Start is called before the first frame update
    void Start () {
        this.counterText = GameObject.Find ("CounterText").GetComponent<Text> ();
    }

    // Update is called once per frame
    void Update () {

        // カウンタテキスト編集
        this.counterText.text = count.ToString () + "/" + successLineCount.ToString ();

        // 成功したら画面遷移
        if (count == successLineCount) {
            SceneManager.LoadScene ("D01_Sucess");
        }

        // Vector3でマウス位置座標を取得する
        position = Input.mousePosition;
        // マウス位置座標をスクリーン座標からワールド座標に変換する
        screenToWorldPointPosition = Camera.main.ScreenToWorldPoint (position);
        // ワールド座標に変換されたマウス座標を代入
        transform.position = new Vector3 (
            screenToWorldPointPosition.x,
            transform.position.y,
            transform.position.z);
    }

    void OnCollisionEnter2D (Collision2D collision) {
        Destroy (collision.gameObject);
        count++;
    }

}